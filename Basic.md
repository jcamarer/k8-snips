## Get Information

Get namespaces
```
kubectl get namespace
```

Get pods under a common namespace, in this example the namespace is `cta405`
```
kubectl get pod --namespace cta405
kubectl get pod -n cta405
```

To see even the hidden pods, add `-a`
```
kubectl get pods -a -n cta405
```

Full information about an specific pod, in this case the pod is `client`.
```
kubectl get pod client -n cta405 -o yaml
kubectl describe pod/client -n cta405
```

## Executing commands

The name of the pod to be accessed is `ctafrontend` and the command to be executed is `cta-catalogue-schema-verify /etc/cta/cta-catalogue.conf`

```
kubectl - cta405 exec ctafrontend -- cta-catalogue-schema-verify /etc/cta/cta-catalogue.conf
```

#### Exectuting the bash to access internally to the pod
```
kubectl --namespace=cta405 exec --stdin --tty ctafrontend -- /bin/bash
```

#### Exectuting command in a container of a multiple container pod

Sometimes a pod (`tpsrv02`) contains several containers (`taped` and `rmcd`), to access an specific container (`taped`) we use `-c`:
```
kubectl --namespace=cta405 exec --stdin --tty tpsrv02 -c taped -- /bin/bash
```

## Copy files to a pod

`client` is a pod.

```
kubectl -n cta405 cp client_helper.sh client:/root/client_helper.sh
```

## Delete Namespace with all the pods

```
kubectl delete namespace cta405
```
